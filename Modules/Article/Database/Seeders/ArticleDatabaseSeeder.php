<?php

namespace Modules\Article\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ArticleDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

         \DB::table('article_settings')->insert([
                [
                    'name' => 'Writer Role', 'type' => 'writer', 'value' => 'Writer'
                ],
                [
                    'name' => 'Writer Head', 'type' => 'writer_head', 'value' => 'Writer Head'
                ],
                [
                    'name' => 'Publisher', 'type' => 'publisher', 'value' => 'Publisher'
                ],
            ]);
         \DB::table('article_types')->insert([
                [
                    'name' => 'Review Article', 'description' => 'Review ARticle Description Here'
                ]
            ]);
         // \DB::table('articles')->insert([
         //          [
         //            'title' => 'Test Article Title', 'creator' => '1', 'creator' => '1', 'publishing' => 'true', 'wordcount' => '200', 'parent_task' => '1'
         //        ]
         //    ]);
    }
}
