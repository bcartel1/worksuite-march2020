<?php

namespace Modules\Article\Entities;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Modules\Article\Entities\ArticleFile;
use Modules\Article\Entities\ArticleComment;

class Article extends Model
{
    protected $table = 'articles';
    protected $guarded = [];

    public function assignee()
    {
        return $this->belongsTo(User::class, 'assignee', 'id');
    }

    public function files()
    {
    	return $this->hasMany(ArticleFile::class, 'article_id', 'id');
    }

    public function comments()
    {
    	return $this->belongsTo(ArticleComment::class, 'article_id', 'id');
    }

}
