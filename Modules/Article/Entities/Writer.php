<?php

namespace Modules\Article\Entities;

use Illuminate\Database\Eloquent\Model;

class Writer extends Model
{
	protected $guarded = [];
	public $writerRole = ArticleSetting::where('type', 'writer')->first()->value;
	public $publisherRole = ArticleSetting::where('type', 'publisher')->first()->value;

	public function writers($roleName ='Writer')
	{
		$this->roleName = $roleName;
        $this->writers = User::withoutGlobalScope('active')->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->select('users.id', 'users.name', 'users.image', 'users.email', 'users.created_at')
        ->where('roles.name',$this->roleName)->get();
		return $this->writers;
	}
}
