<?php

namespace Modules\Article\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Requests;
use App\Http\Controllers\Admin\AdminBaseController;
use Modules\Article\Entities\Article;
use Modules\Article\Entities\ArticleType;
use Modules\Article\Entities\ArticleSetting;
use Modules\Article\Entities\Writer;
use Modules\Article\Entities\ArticleFile;
use App\Helper\Reply;
use Illuminate\Http\File;
use App\User;
use App\Setting;
use App\RoleUser;
use App\Role;
use App\Task;

class AdminArticleController extends AdminBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('Article Management');
        $this->pageIcon = 'ti-pencil';
        $this->global = Setting::first();
        $this->user = user();
    }

    public function getWriters()
    {
        $this->roleName = ArticleSetting::where('type', 'writer')->first()->value;
        $this->writers = $this->writers = User::withoutGlobalScope('active')->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->select('users.id', 'users.name', 'users.image', 'users.email', 'users.created_at')
        ->where('roles.name',$this->roleName)->get();
        return $this->writers;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $this->articles = Article::all();
        return view('article::index', $this->data);
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {
        $this->tasks = Task::all();
        $this->articleTypes = ArticleType::all();
        $this->writers = $this->getWriters();
        $this->publishers = $this->getWriters();
        return view('article::create', $this->data);
    }

    /**
     * Store files in storage.
     * @param Request $request
     * @return Response
     */
    public function storeFiles(Request $request)
    {
        $articles = explode(',', $request->articles);
        if ($request->hasFile('file')) {
            $fileData = $request->file('file');
            $filename = $fileData->getClientOriginalName();
            $ext = strtolower(\File::extension($filename));
            $hashname = md5(microtime()).'.'.$ext;

            for ($i=0; $i <count($articles) ; $i++) {
                $file = new ArticleFile();
                $file->user_id = auth()->id();
                $file->article_id = $request->articles[$i];
                $file->filename = $filename;
                $file->hashname = $hashname;
                $file->size = $fileData->getSize();
                $file->save();
            }

            $fileData->move(public_path('user-uploads/article-files'), $hashname);

            return Reply::success('article::app.storeNewArticle');
        }

        return Reply::success('article::app.storeNewArticle');
    }

        /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
        public function store(Request $request)
        {
            $count = count($request->title);
            for ($i=0; $i < $count; $i++) {
                if ($request->title[$i] ==null || $request->type[$i] ==null || $request->word_count[$i] ==null || $request->rate[$i] ==null || $request->description ==null || $request->writing_deadline ==null || $request->assignee ==null || $request->priority ==null) {
                    return Reply::error('article::app.storeNewArticleError');
                } else {
                    if (isset($request->publishing[$i])) {
                        $publishing = 1;
                    } else {
                        $publishing = 0;
                    }
                    if ($request->parent !=null){
                        $parent_task = $request->parent_task;
                    } else {
                        $parent_task = null;
                    }

                    $article = new Article([
                        'title' => $request->title[$i],
                        'type' => $request->type[$i],
                        'word_count' => $request->word_count[$i],
                        'rate' => $request->rate[$i],
                        'publishing' => $publishing,
                        'description' => $request->description,
                        'parent_task' => $parent_task,
                        'writing_deadline' => $request->writing_deadline,
                        'assignee' => $request->assignee,
                        'creator' => auth()->id(),
                        'priority' => $request->priority

                    ]);
                    $article->save();
                    $article_id[] = $article->id;
                }
            }
            return  Reply::successWithData(__('Article assigned, checking attachements!'), ['articles' => $article_id]);
        }

    /**
     * Show the list of writers.
     * @return Response
     */
    public function writers($exceptId = NULL)
    {
        $this->writers = $this->getWriters();
        $this->totalWriters = count($this->writers);
        return view('article::writers', $this->data);
    }

     /**
     * Show details of writers.
     * @return Response
     */
     public function writerView($id)
     {
        $this->writer = User::findOrFail($id);
        return view('article::writerView', $this->data);
    }

    /**
     * Show the preferences of Articles Management System.
     * @return Response
     */
    public function settings()
    {
        $this->settings = ArticleSetting::all();
        $this->writerRole = ArticleSetting::where('type', 'writer')->first();
        $this->writerHeadRole = ArticleSetting::where('type', 'writer_head')->first();
        $this->roles = Role::all();
        return view('article::settings', $this->data);
    }

    /**
     * Store settings.
     * @param Request $request
     * @return Response
     */
    public function storeSettings(Request $request)
    {
        $writer = ArticleSetting::where('type', 'writer')->first();
        if ($writer ==null) {
            $writer = new ArticleSetting;
        }
        $writerHead = ArticleSetting::where('type', 'writer_head')->first();
        $writer->value =  $request->writer;
        $writer->save();
        $writerHead->value =  $request->writer_head;
        $writerHead->save();
        return Reply::redirect(route('admin.article.settings'), 'article::app.storeSettingSuccess');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function typeView(Request $request)
    {
        $this->types = ArticleType::all();
        return view('article::createType', $this->data);
    }


    /**
     * Store the form for creating a new resource.
     * @return Response
     */
    public function createType(Request $request)
    {
        $name = $request->name;
        $description = $request->description;
        if ($name ==null) {
            return Reply::error('article::app.storeTypeEmptyName');
        }
        if ($description ==null) {
            return Reply::error('article::app.storeTypeEmptyDescription');
        }
        $type = ArticleType::create([
            'name' => $name,
            'description' => $description
        ]);
        $type->save();
        return Reply::success('article::app.storeTypeSuccess');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);
        $task = Task::find($article->parent_task);
        $type = ArticleType::find($article->type);

        $view = view('article::show', ['article' => $article, 'type' => $type, 'task' => $task])->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('article::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
