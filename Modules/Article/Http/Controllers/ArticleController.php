<?php

namespace Modules\Article\Http\Controllers;
use App\Http\Controllers\Member\MemberBaseController;
use Modules\Article\Entities\Article;
use Modules\Article\Entities\ArticleType;
use Modules\Article\Entities\ArticleSetting;
use Modules\Article\Entities\Writer;
use Modules\Article\Entities\ArticleFile;
use Illuminate\Http\Response;
use App\Helper\Reply;
use Illuminate\Http\File;
use App\User;
use App\Setting;
use App\RoleUser;
use App\Role;
use App\Task;
use Pusher\Pusher;

class ArticleController extends MemberBaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->pageTitle = __('article::app.menu.article');
        $this->pageIcon = 'icon-pencil';
    }

    //Writers
    public function getWriters()
    {
        $this->roleName = ArticleSetting::where('type', 'writer')->first()->value;
        $this->writers = User::withoutGlobalScope('active')->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->select('users.id', 'users.name', 'users.image', 'users.email', 'users.created_at')
        ->where('roles.name',$this->roleName)->get();
        return $this->writers;
    }

    //Publishers
    public function getPublishers()
    {
        $this->roleName = ArticleSetting::where('type', 'publisher')->first()->value;
        $this->publishers = User::withoutGlobalScope('active')->join('role_user', 'role_user.user_id', '=', 'users.id')
        ->join('roles', 'roles.id', '=', 'role_user.role_id')
        ->select('users.id', 'users.name', 'users.image', 'users.email', 'users.created_at')
        ->where('roles.name',$this->roleName)->get();
        return $this->publishers;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $details = ['logo_url' => '', 'company_name' => 'Viser X'];
        $pageTitle ="Article Management";
        $this->articles = Article::all();
        return view('article::index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->tasks = Task::all();
        $this->articleTypes = ArticleType::all();
        $this->writers = $this->getWriters();
        $this->publishers = $this->getPublishers();
        return view('article::create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $count = count($request->title);
        for ($i=0; $i < $count; $i++) {
            if ($request->title[$i] ==null || $request->type[$i] ==null || $request->word_count[$i] ==null || $request->rate[$i] ==null || $request->description ==null || $request->writing_deadline ==null || $request->assignee ==null || $request->priority ==null) {
                return Reply::error('article::app.storeNewArticleError');
            } else {
                if (isset($request->publishing[$i])) {
                    $publishing = 1;
                } else {
                    $publishing = 0;
                }
                if ($request->parent !=null){
                    $parent_task = $request->parent_task;
                } else {
                    $parent_task = null;
                }

                $article = new Article([
                    'title' => $request->title[$i],
                    'type' => $request->type[$i],
                    'word_count' => $request->word_count[$i],
                    'rate' => $request->rate[$i],
                    'publishing' => $publishing,
                    'description' => $request->description,
                    'parent_task' => $parent_task,
                    'writing_deadline' => $request->writing_deadline,
                    'assignee' => $request->assignee,
                    'creator' => auth()->id(),
                    'priority' => $request->priority

                ]);
                $article->save();
                // $user = User::findOrFail($request->assignee);
                // $user->notify(new NewArticle($article));
                $article_id[] = $article->id;
            }
        }
        return  Reply::successWithData('Article assigned, checking attachements!', ['articles' => implode(',',$article_id)]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);
        $task = Task::find($article->parent_task);
        $type = ArticleType::find($article->type);
        $files = ArticleFile::where('article_id',$article->id)->get();

        $view = view('article::show', ['article' => $article, 'type' => $type, 'task' => $task, 'files' => $files])->render();
        return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $this->tasks = Task::all();
        $this->article = Article::findOrFail($id);
        $this->task = Task::find($this->article->parent_task);
        $this->type = ArticleType::find($this->article->type);
        $this->files = ArticleFile::where('article_id',$this->article->id)->get();
        $this->writers = $this->getWriters();
        $this->publishers = $this->getPublishers();
        $this->articleTypes = ArticleType::all();

        return view('article::edit', $this->data);
        //return Reply::dataOnly(['status' => 'success', 'view' => $view]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->article = Article::findOrFail($id);
        $this->article->title = $request->title;
        $this->article->type = $request->type;
        $this->article->word_count = $request->word_count;
        $this->article->rate = $request->rate;
        if (isset($request->publishing)) {
            $this->article->publishing = 1;
        } else {
            $this->article->publishing = 0;
        }
        $this->article->description = $request->description;
        if (isset($request->parent)) {
            $this->article->parent_task = $request->parent_task;
        } else {
            $this->article->parent_task = null;
        }
        $this->article->writing_status = $request->writing_status;
        $this->article->assignee = $request->assignee;
        $this->article->priority = $request->priority;
        $this->article->save();

        return Reply::success('article::app.updateArticle');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function updateStatus(Request $request, $id)
    {
        $article = Article::findOrFail($id);
        $article->writing_status = $request->status;
        $article->save();
        return Reply::success('article::app.updateArticle');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Article::findOrFail($id)->delete();
        
        return Reply::success('Article deleted!');
    }
}

