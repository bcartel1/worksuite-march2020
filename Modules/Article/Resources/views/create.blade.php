<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title"><i class="ti-plus"></i> Assign New Articles</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'storeArticle','class'=>'ajax-form','method'=>'POST', 'files' => true]) !!}

        <div class="form-body">
            <div class="row">
                <div id="article-tab">

                </div>
                <div class="form-group row">
                    <div class="col-md-3">
                        <input type="number" name="article_count" class="form-control" id="article_count" value="1">
                    </div>
                    <div class="col-md-9">
                        <button type="button" class="btn btn-success" id="add-more"><i class="fa fa-plus"></i> Add More</button>
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label required">@lang('app.description')</label>
                        <textarea rows="4" name="description" class="summernote"></textarea>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group" id="file-upload-tab">
                        <label>Attachment</label>
                        <input id="file-upload" type="file" name="file" class="form-control">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="checkbox checkbox-info">
                            <input id="parent" name="parent" value="yes"
                            type="checkbox">
                            <label for="parent" class="control-label">Parent</label>
                        </div>
                    </div>
                </div>

                <div class="row" id="parent-field" style="display: none">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label required">Parent Task</label>
                            <select class="select2 form-control" data-placeholder="Parent Task" name="parent_task" id="parent_task" >
                                <option value=""></option>
                                @foreach ($tasks as $task)
                                <option value="{{$task->id}}">{{$task->heading}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label required">@lang('app.dueDate')</label>
                        <input type="text" name="writing_deadline" id="writing_deadline" class="form-control" autocomplete="off">
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label required">@lang('modules.tasks.assignTo')</label>
                        <select class="select2 form-control" data-placeholder="@lang('modules.tasks.chooseAssignee')" name="assignee" id="assignee" >
                            <option value=""></option>
                            @foreach ($writers as $writer)
                            <option value="{{$writer->id}}">{{$writer->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label required">@lang('modules.tasks.priority')</label>

                        <div class="radio radio-danger">
                            <input type="radio" name="priority" id="radio13"
                            value="high">
                            <label for="radio13" class="text-danger">
                            @lang('modules.tasks.high') </label>
                        </div>
                        <div class="radio radio-warning">
                            <input type="radio" name="priority"
                            id="radio14" checked value="medium">
                            <label for="radio14" class="text-warning">
                            @lang('modules.tasks.medium') </label>
                        </div>
                        <div class="radio radio-success">
                            <input type="radio" name="priority" id="radio15"
                            value="low">
                            <label for="radio15" class="text-success">
                            @lang('modules.tasks.low') </label>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <!-- <input type="hidden" id="articlesID" name="articles" value=""> -->

            </div>
            <!--/row-->

        </div>
        <div class="form-actions">
            <button type="button" id="store-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
        </div>

        {!! Form::close() !!}
    </div>
</div>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.js') }}"></script>

<script>
    $('#add-more').click(function(){
        var article_count = $('#article_count').val();
        for (var i = 0; i < article_count; i++) {

            var article_tab = '<div id="article-tab"> <div class="col-md-12"> <div class="form-group"> <label class="control-label required">@lang("app.title")</label> <input type="text" id="title" name="title[]" class="form-control" > </div> </div> <div class="col-md-4"> <div class="form-group"> <label class="control-label required">Type</label> <select class="select2 form-control" data-placeholder="Type" name="type[]" id="type" > <option value=""></option> @foreach ($articleTypes as $articleType) <option value="{{$articleType->id}}">{{$articleType->name}}</option> @endforeach </select> </div> </div> <div class="col-md-3"> <div class="form-group"> <label class="control-label required">Word Count</label> <input type="number" name="word_count[]" class="form-control"> </div> </div> <div class="col-md-3"> <div class="form-group"> <label class="control-label required">Rate</label> <input type="number" name="rate[]" class="form-control"> </div> </div> <div class="col-md-2"> <div class="form-group align-middle"> <label>Publishing</label> <div class="checkbox checkbox-info"> <input id="'+i+'" name="publishing[]" value="true" type="checkbox"> <label for="'+i+'">Yes</label> </div> </div> </div> <div class="col-md-12"><hr></div> </div>';

            $('#article-tab').append(article_tab);
            $('#add-more').hide();
            $('#article_count').hide();
        }
    })

    //    update task
    $('#store-task').click(function () {
        $.easyAjax({
            url: "{{route('member.article.store')}}",
            container: '#storeArticle',
            type: "POST",
            data: $('#storeArticle').serialize(),
            success: function (res) {
                if (res.status ==='success') {
                    var CSRF_TOKEN = '{{ csrf_token() }}';
                    var formData = new FormData();
                    formData.append('articles', res.articles);
                    formData.append('file', $('#file-upload')[0].files[0]);
                    $.ajax({
                        url:  "{{route('member.article.storeFiles')}}?_token="+CSRF_TOKEN,
                        type: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            if (result.status ='success') {
                                location.reload(true);
                                $.showToastr(result.message, 'success');
                            }
                        }
                    })
                }
            }
        });
    });

    jQuery('#writing_deadline').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });

    $("#parent_task").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    // $("#type").select2({
    //     formatNoMatches: function () {
    //         return "{{ __('messages.noRecordFound') }}";
    //     }
    // });

    $("#assignee").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('#parent').change(function () {
        if($(this).is(':checked')){
            $('#parent-field').show();
        }
        else{
            $('#parent-field').hide();
        }
    });
</script>
<script type="text/javascript">
    $('.summernote').summernote({
        height: 100,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
            ]
        });
    </script>

