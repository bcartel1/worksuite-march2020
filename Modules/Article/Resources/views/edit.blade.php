<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/dropzone-master/dist/dropzone.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 class="modal-title"><i class="ti-plus"></i> Edit Article</h4>
</div>
<div class="modal-body">
    <div class="portlet-body">

        {!! Form::open(['id'=>'updateArticle','class'=>'ajax-form','method'=>'POST', 'files' => true]) !!}

        <div class="form-body">
            <div class="row">
                <div id="article-tab">
                </div>
                <div class="form-group row">
                    <div id="article-tab"> 
                        <div class="col-md-12"> 
                            <div class="form-group"> 
                                <label class="control-label required">@lang("app.title")
                                </label> 
                                <input type="text" id="title" name="title" class="form-control" value="{{$article->title}}"> 
                            </div> 
                        </div> 
                        <div class="col-md-4"> 
                            <div class="form-group"> 
                                <label class="control-label required">Type
                                </label> 
                                <select class="select2 form-control" data-placeholder="Type" name="type" id="type" > 
                                    <option value="{{$article->type}}">{{Modules\Article\Entities\ArticleType::find($article->type)->name}}</option> 
                                    @foreach ($articleTypes as $articleType) 
                                    <option value="{{$articleType->id}}">{{$articleType->name}}</option> @endforeach 
                                </select> 
                            </div> 
                        </div> 
                        <div class="col-md-3"> 
                            <div class="form-group"> 
                                <label class="control-label required">Word Count
                                </label> 
                                <input type="number" name="word_count" class="form-control" value="{{$article->word_count}}"> 
                            </div> 
                        </div> 
                        <div class="col-md-3"> 
                            <div class="form-group"> 
                                <label class="control-label required">Rate
                                </label> 
                                <input type="number" name="rate" class="form-control" value="{{$article->rate}}"> 
                            </div> 
                        </div> 
                        <div class="col-md-2"> 
                            <div class="form-group align-middle"> 
                                <label>Publishing
                                </label> 
                                <div class="checkbox checkbox-info"> 
                                    <input id="publishing" name="publishing" value="@if ($article->publishing ==1) false @else true @endif" type="checkbox" @if ($article->publishing ==1) checked @endif> 
                                    <label for="publishing">Yes
                                    </label> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="col-md-12">
                            <hr>
                        </div> 
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label required">@lang('app.description')</label>
                        <textarea rows="4" name="description" class="summernote">{{$article->description}}</textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group" id="file-upload-tab">
                        <label>Attachment</label>
                        <input id="file-upload" type="file" name="file" class="form-control">
                    </div>
                </div>
                <div class="col-xs-12 m-t-10">Files: </div>
                @forelse ($files as $file)
                <div class="col-xs-12 m-t-10">
                    <a href="javascript:;" onclick="downloadFile('{{$file->id}}')" class="btn btn-primary btn-sm btn-rounded btn-outline"><i class="fa fa-file"></i> {{$file->filename}} ({{number_format($file->size / 1024, 2)}} KB)</a> <a href="javascript:;" onclick="deleteFile('{{$file->id}}')" id="delete-{{$file->id}}" class="btn btn-danger btn-sm btn-rounded btn-outline"><i class="fa fa-trash"></i></a>
                </div>
                @empty
                <div class="col-xs-12 m-t-10">
                    <span id="totalUploadedFiles">No files uploaded!</span>
                </div>
                @endforelse

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="checkbox checkbox-info">
                            <input id="parent" name="parent" value="yes"
                            type="checkbox" @if ($article->parent_task !=null) checked @endif>
                            <label for="parent" class="control-label">Parent</label>
                        </div>
                    </div>
                </div>

                <div class="row" id="parent-field" @if ($article->parent_task ==null)style="display: none" @endif>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label required">Parent Task</label>
                            <select class="select2 form-control" data-placeholder="Parent Task" name="parent_task" id="parent_task" >
                                @if (isset($task->id))
                                <option value="{{$task->id}}">{{$task->heading}}</option>
                                @endif
                                @foreach ($tasks as $task)
                                <option value="{{$task->id}}">{{$task->heading}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label required">@lang('app.dueDate')</label>
                        <input type="text" name="writing_deadline" id="writing_deadline" class="form-control" autocomplete="off" value="{{$article->writing_deadline}}">
                    </div>
                </div>
                <!--/span-->
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label required">@lang('modules.tasks.assignTo')</label>
                        <select class="select2 form-control" data-placeholder="@lang('modules.tasks.chooseAssignee')" name="assignee" id="assignee" >
                            <option value="{{$article->assignee}}">{{App\User::find($article->assignee)->name}}</option>
                            @foreach ($writers as $writer)
                            <option value="{{$writer->id}}">{{$writer->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label required">@lang('modules.tasks.priority')</label>

                        <div class="radio radio-danger">
                            <input type="radio" name="priority" id="radio13"
                            value="high" @if ($article->priority =='high') checked @endif>
                            <label for="radio13" class="text-danger">
                            @lang('modules.tasks.high') </label>
                        </div>
                        <div class="radio radio-warning">
                            <input type="radio" name="priority"
                            id="radio14" value="medium" @if ($article->priority =='medium') checked @endif>
                            <label for="radio14" class="text-warning">
                            @lang('modules.tasks.medium') </label>
                        </div>
                        <div class="radio radio-success">
                            <input type="radio" name="priority" id="radio15"
                            value="low" @if ($article->priority =='low') checked @endif>
                            <label for="radio15" class="text-success">
                            @lang('modules.tasks.low') </label>
                        </div>
                    </div>
                </div>
                <!--/span-->
            </div>
            <!--/row-->

        </div>
        <div class="form-actions">
            <button type="button" id="edit-task" class="btn btn-success"><i class="fa fa-check"></i> @lang('app.save')</button>
            <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
        </div>

        {!! Form::close() !!}
    </div>
</div>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

<script>
    //    update task
    $('#edit-task').click(function () {
        var id = "{{$article->id}}";
        var url = "{{route('member.article.update', ':id')}}";
        var url = url.replace(':id', id);
        $.easyAjax({
            url: url,
            container: '#updateArticle',
            type: "POST",
            data: $('#updateArticle').serialize(),
            success: function (res) {
                var file = $('#file-upload')[0].files[0];
                if (file !=null) {
                    var CSRF_TOKEN = '{{ csrf_token() }}';
                    var formData = new FormData();
                    formData.append('articles', id);
                    formData.append('file', file);
                    $.ajax({
                        url:  "{{route('member.article.storeFiles')}}?_token="+CSRF_TOKEN,
                        type: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            if (result.status ='success') {
                                $.showToastr('File uploaded successfully!', 'success');
                            }
                        }
                    });
                }
                $("#subTaskModal").modal('toggle');
                var url = "{{ route('member.article.show',':id') }}";
                url = url.replace(':id', id);
                $.easyAjax({
                    type: 'GET',
                    url: url,
                    success: function (response) {
                        if (response.status == "success") {
                            $('#right-sidebar-content').html(response.view);
                        }
                    }
                });
                
            }
        })
    });

    function deleteFile(id){
        var url = "{{route('member.article.removeArticle', ':id')}}?_token={{csrf_token()}}";
        var url = url.replace(':id', id);
        $.easyAjax({
            url: url,
            type: "POST",
            success: function (res) {
                $('#delete-'+id).parent().hide();
            }
        });
    }

    jQuery('#writing_deadline').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true
    });

    $("#parent_task").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $("#type").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $("#assignee").select2({
        formatNoMatches: function () {
            return "{{ __('messages.noRecordFound') }}";
        }
    });

    $('#parent').change(function () {
        if($(this).is(':checked')){
            $('#parent-field').show();
        }
        else{
            $('#parent-field').hide();
        }
    })
</script>
<script type="text/javascript">
    $('.summernote').summernote({
        height: 100,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
            ]
        })
    </script>
