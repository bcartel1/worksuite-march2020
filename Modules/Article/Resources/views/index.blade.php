@extends('article::layouts.app')

@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-8 col-md-5 col-sm-6 col-xs-12">
        <h4 class="page-title"><i class="{{ $pageIcon ?? '' }}"></i> {{ $pageTitle ?? '' }}
            <span class="text-info b-l p-l-10 m-l-5">{{ $totalAssets ?? '' }}</span> <span
            class="font-12 text-muted m-l-5"> @lang('article::app.totalArticles')</span>
        </h4>
    </div>

    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-4 col-sm-6 col-md-7 col-xs-12 text-right">
        <a href="javascript:;" id="createArticle" 
        class="btn btn-outline btn-success btn-sm">@lang('article::app.addNewArticle') <i class="fa fa-plus"
        aria-hidden="true"></i></a>
        <ol class="breadcrumb">
            <li><a href="{{ route('member.dashboard') }}">@lang('app.menu.home')</a></li>
            <li class="active">{{ $pageTitle ?? '' }}</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection

@push('head-script')
<style>
    .swal-footer {
        text-align: center !important;
    }
    #allTasks-table_wrapper .dt-buttons{
        display: none !important;
    }
</style>
@endpush

@section('content')
<div class="row">

    <div class="col-md-12">
        <div class="white-box">
            @section('filter-section')
            <div class="row">
                <form action="">
                    <div class="col-md-12">
                        <div class="form-group">
                         <h5>@lang('article::app.articleType')</h5>
                         <select class="select2 form-control" name="asset_type_id" id="asset_type_id" data-style="form-control">
                            <option value="all">@lang('modules.client.all')</option>

                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <h5>@lang('article::app.employees')</h5>
                        <select class="select2 form-control" name="user_id" id="user_id" data-style="form-control">
                            <option value="all">@lang('modules.client.all')</option>

                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <h5>@lang('article::app.status')</h5>
                        <select class="select2 form-control" name="status" id="status" data-style="form-control">
                            <option value="all">@lang('modules.client.all')</option>

                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group p-t-10">
                        <label class="control-label col-xs-12">&nbsp;</label>
                        <button type="button" id="apply-filters" class="btn btn-success btn-sm col-md-6"><i
                            class="fa fa-check"></i> @lang('app.apply')</button>
                            <button type="button" id="reset-filters"
                            class="btn btn-inverse col-md-5 btn-sm col-md-offset-1"><i
                            class="fa fa-refresh"></i> @lang('app.reset')</button>
                        </div>
                    </div>
                </form>
            </div>
            @endsection

            <table class="table table-bordered table-hover toggle-circle default footable-loaded footable dataTable no-footer dtr-inline">
                <thead>
                    <tr role="row">
                        <th>#</th>
                        <th>Title</th>
                        <th>Assignee</th>
                        <th>Creator</th>
                        <th>Word Count</th>
                        <th>Priority</th>
                        <th>Deadline</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($articles as $article)
                    <tr role="row" class="odd">
                        <td>{{$article->id}}</td>
                        <td>
                            <a href="javascript:;" id="{{$article->id}}" onclick="view('{{$article->id}}')">{{$article->title}}</a>
                        </td>
                        <td>{{App\User::find($article->assignee)->name}}</td>
                        <td>{{App\User::find($article->creator)->name}}</td>
                        <td>{{$article->word_count}}</td>
                        <td><div class="label @if($article->priority =='low') label-success @elseif($article->priority =='medium') label-warning @else label-danger @endif">@if($article->priority =='low') Low @elseif($article->priority =='medium') Medium @else High @endif</div></td>
                        <td>{{$article->writing_deadline}}</td>
                        <td class=" text-center">
                            <div class="btn-group dropdown m-r-10">
                                <button aria-expanded="false" data-toggle="dropdown" class="btn dropdown-toggle waves-effect waves-light" type="button"><i class="ti-more"></i></button>
                                <ul role="menu" class="dropdown-menu pull-right">
                                    <li><a href="javascript:;" onclick="editArticle('{{$article->id}}')"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a></li>
                                    <li><a href="javascript:;" onclick="view('{{$article->id}}')"><i class="fa fa-search" aria-hidden="true"></i> View</a></li>
                                    <li><a href="javascript:;" onclick="deleteArticle('{{$article->id}}')" class="sa-params"><i class="fa fa-times" aria-hidden="true"></i> Delete</a></li>
                                </ul> 
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>  


<!-- Modal -->
<div class="modal fade" id="createType" tabindex="-1" role="dialog" aria-labelledby="createTypeModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        @lang('article::app.addArticleType')
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <form method="post" id="createTypeForm" action="{{route('member.article.createType')}}">
    @csrf
    <div class="modal-body">
        <label for="name"></label>
        <input type="text" id="name" class="form-control" name="name" placeholder="Name">
        <label for="description"></label>
        <textarea name="description" id="description" class="form-control" placeholder="Description"></textarea>
    </div>
</form>
<div class="modal-footer">
    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
    <button class="btn btn-success" id="save-form">Save changes</button>
</div>
</div>
</div>
</div>

{{--Ajax Modal--}}
<div class="modal fade bs-modal-md in"  id="subTaskModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" id="modal-data-application">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <span class="caption-subject font-red-sunglo bold uppercase" id="subTaskModelHeading">Loading...</span>
            </div>
            <div class="modal-body">
                Loading...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
                <button type="button" class="btn blue">Save changes</button>
            </div>
        </div>
    </div>
</div>
{{--Ajax Modal Ends--}}

@endsection

@push('footer-script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    // $('#save-form').click(function () {
    //     $.easyAjax({
    //         url: '{{route('member.article.createType')}}',
    //         container: '#createTypeForm',
    //         type: "POST",
    //         redirect: true,
    //         file:true,
    //         dataType:'json',
    //         success: function (data) {
    //             $('#createTypeForm').trigger("reset");
    //         }
    //         // data: $('#createAsset').serialize()
    //     })
    // })

    function view(id) {
        $(".right-sidebar").slideDown(50).addClass("shw-rside");
        var url = "{{ route('member.article.show',':id') }}";
        url = url.replace(':id', id);

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response.status == "success") {
                    $('#right-sidebar-content').html(response.view);
                }
            }
        });
    }

    $('#createArticle').click(function () {
        var url = "{{ route('member.article.create') }}";
        $.ajaxModal('#subTaskModal', url);
    })

    function editArticle(id) {
        var url = "{{ route('member.article.edit',':id') }}";
        url = url.replace(':id', id);
        $.ajaxModal('#subTaskModal', url);
    }

    function deleteArticle(id) {
        var buttons = {
            cancel: "Cancel",
            confirm: {
                text: "Yes",
                value: 'confirm',
                visible: true,
                className: "danger",
            }
        };
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover the deleted task!",
            dangerMode: true,
            icon: 'warning',
            buttons: buttons,
        }).then(function (isConfirm) {
            if (isConfirm ==='confirm') {
                var url = "{{ route('member.article.delete',':id') }}";
                url = url.replace(':id', id);
                var token = "{{ csrf_token() }}";
                var dataObject = {'_token': token, '_method': 'DELETE'};
                $.easyAjax({
                    type: 'POST',
                    url: url,
                    data: dataObject,
                    success: function (response) {
                        if (response.status == "success") {
                            $.unblockUI();
                            location.reload(true);
                        }
                    }
                });
            }
        });
    }

$( document ).ready(function(){
        var req = '{{request('view-article')}}';
        if (req !='') {
            $(".right-sidebar").slideDown(50).addClass("shw-rside");
        var url = "{{ route('member.article.show',':id') }}";
        url = url.replace(':id', req);

        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response.status == "success") {
                    $('#right-sidebar-content').html(response.view);
                }
            }
        });
        }
    });
</script>
@endpush
