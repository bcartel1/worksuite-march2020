<li><a href="javascript:;" class="waves-effect"><i class="ti-pencil"></i> <span class="hide-menu"> @lang('article::app.menu.article')<span class="fa arrow"></span> </span></a>
	<ul class="nav nav-second-level collapse">
		<li><a href="{{route('member.article.index')}}" class="waves-effect">
			<span class="hide-menu">@lang('article::app.articles')</span></a>
		</li>
		<li><a href="{{route('member.article.writers')}}" class="waves-effect">
			<span class="hide-menu">@lang('article::app.writers')</span></a>
		</li>
		<li><a href="{{route('member.article.settings')}}" class="waves-effect">
			<span class="hide-menu">@lang('article::app.writers')</span></a>
		</li>
	</ul>
</li>