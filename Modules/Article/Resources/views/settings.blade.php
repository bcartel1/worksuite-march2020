@extends('layouts.app')

@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-8 col-md-5 col-sm-6 col-xs-12">
        <h4 class="page-title"><i class="{{ $pageIcon ?? '' }}"></i> {{ $pageTitle ?? '' }}
        </h4>
    </div>
    <div class="col-lg-4 col-sm-6 col-md-7 col-xs-12 text-right">
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li class="active">{{ $pageTitle ?? '' }}</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection

@push('head-script')
<link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/bower_components/custom-select/custom-select.css') }}">
<link rel="stylesheet"
href="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<style type="text/css">
    .filter-section>h5{
        display:none;
    }
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="white-box">

            @section('filter-section')
            <div class="row">
                <h1>@lang('article::app.settings')</h1>
            </div>
            @endsection

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <form method="post" id="settings">
                            @csrf
                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">Writer Role</label>
                            <div class="col-md-9">
                                <select class="form-control custom-select" name="{{$writerRole->type}}">
                                    <option value="{{$writerRole->value}}">--Select--</option>
                                    @foreach ($roles as $role)
                                    <option value="{{$role->name}}">{{$role->display_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">Writer Head Role</label>
                            <div class="col-md-9">
                                <select class="form-control custom-select" name="{{$writerHeadRole->type}}">
                                    <option value="{{$writerHeadRole->value}}">--Select--</option>
                                    @foreach ($roles as $role)
                                    <option value="{{$role->name}}">{{$role->display_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                        <button class="btn btn-success waves-effect waves-light m-r-10" id="save-form">Save</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
@endsection

@push('footer-script')
<script src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js"></script>
<script src="{{ asset('plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript">
     $('#save-form').click(function () {
        $.easyAjax({
            url: '{{route('admin.article.settings')}}',
            container: '#settings',
            type: "POST",
            redirect: true,
            file:true
            // data: $('#createAsset').serialize()
        })
    });
</script>
@endpush
