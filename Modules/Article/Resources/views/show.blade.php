<link rel="stylesheet" href="{{ asset('plugins/bower_components/summernote/dist/summernote.css') }}">


<div class="rpanel-title"> {{$article->title}} <div class="label @if($article->priority =='low') label-success @elseif($article->priority =='medium') label-warning @else label-danger @endif">@if($article->priority =='low') Low @elseif($article->priority =='medium') Medium @else High @endif</div> <span><i class="ti-close right-side-toggle"></i></span> </div>
<div class="r-panel-body">

    <div class="row">
        <div class="col-xs-12">
            @if (auth()->id() == $article->creator)
            <a href="javascript:;" class="btn btn-danger btn-sm m-b-10 btn-rounded btn-outline pull-right m-l-5" onclick="deleteArticle('{{$article->id}}')"> <i class="fa fa-trash"></i> Delete</a>
            <a href="javascript:;" class="btn btn-info btn-sm m-b-10 btn-rounded btn-outline pull-right m-l-5" onclick="editArticle('{{$article->id}}')"> <i class="fa fa-edit"></i> Edit</a>
            @endif

            @if($article->writing_status ==0)
            <a href="javascript:;" id="completedButton" class="btn btn-success btn-sm m-b-10 btn-rounded btn-outline"  onclick="markComplete('complete')" ><i class="fa fa-check"></i> @lang('modules.tasks.markComplete')</a>

            <a href="javascript:;" id="reminderButton" class="btn btn-info btn-sm m-b-10 btn-rounded btn-outline pull-right" title="@lang('messages.remindToAssignedEmployee')"><i class="fa fa-envelope"></i> @lang('modules.tasks.reminder')</a>

            @else
            <a href="javascript:;" id="inCompletedButton" class="btn btn-default btn-outline btn-sm m-b-10 btn-rounded"  onclick="markComplete('incomplete')"><i class="fa fa-times"></i> @lang('modules.tasks.markIncomplete')</a>
            @endif
        </div>
        <div class="col-xs-12">
            <p><i class="ti-layers"> Type:</i> {{$type->name}}</p>
            <p><i class="ti-write"> Word Count:</i> {{$article->word_count}}</p>
            <p><i class="fa fa-money"> Rate:</i> {{$article->rate}}</p>
            @if (isset($task))
            <p><i class="ti-layout-list-thumb"> Parent Task:</i> @if ($task !=null) <a href="javascript:;" onclick="viewTask('{{$task->id}}')" id="task"> {{$task->heading}} </a> @else No parent task! @endif </p>
            @endif
        </div>

        <div class="col-xs-12" id="task-detail-section">
            <div class="row">
                @if ($article->writing_status ==0)
                <div class="col-xs-6 col-md-3 font-12 m-t-10">
                    <label class="font-12" for="">@lang('modules.tasks.assignTo') Writer</label><br>
                    <img src="{{asset_url('/avatar/'.App\User::find($article->assignee)->image)}}" data-toggle="tooltip" data-original-title="{{asset_url('/avatar/'.App\User::find($article->assignee)->image)}}" data-placement="right" class="img-circle" width="25" height="25" alt="">
                    {{App\User::find($article->assignee)->name}}
                </div>
                @endif
                <div class="col-xs-6 col-md-3 font-12 m-t-10">
                    <label class="font-12" for="">@lang('modules.tasks.assignBy')</label><br>
                    <img src="{{asset_url('/avatar/'.App\User::find($article->creator)->image)}}" class="img-circle" width="25" height="25" alt="">

                    {{App\User::find($article->creator)->name}}
                </div>
                @if ($article->publishing ==1 && $article->writing_status ==1)
                @if ($article->publisher !=null)
                <div class="col-xs-6 col-md-3 font-12 m-t-10">
                    <label class="font-12" for="">@lang('modules.tasks.assignTo') Publisher</label><br>
                    <img src="{{asset_url('/avatar/'.App\User::find($article->publisher)->image)}}" class="img-circle" width="25" height="25" alt="">

                    {{App\User::find($article->publisher)->name}}
                </div>
                @else
                <div class="col-xs-6 col-md-3 font-12 m-t-10">
                    <label class="font-12" for="">@lang('modules.tasks.assignTo') Publisher</label><br>
                    Waiting for assign.
                </div>
                @endif
                @endif

                @if($article->writing_status ==0)
                <div class="col-xs-6 col-md-3 font-12 m-t-10">
                    <label class="font-12" for="">Writing @lang('app.dueDate')</label><br>
                    <span class="text-danger">
                        {{$article->writing_deadline}}
                    </span>
                </div>
                @endif

                @if($article->publishing ==1 && $article->writing_status ==1)
                <div class="col-xs-6 col-md-3 font-12 m-t-10">
                    <label class="font-12" for="">Publishing @lang('app.dueDate')</label><br>
                    <span class="text-danger">
                        {{$article->publishing_deadline}}
                    </span>
                </div>
                @endif

                <div class="col-xs-12">
                    <br>
                    <i class="ti-align-justify"> Description:</i>
                    <br>
                    {!! $article->description !!}
                </div>
                @forelse ($files as $file)
                <div class="col-xs-12 m-t-10">
                    <a href="javascript:;" onclick="downloadFile('{{$file->id}}')" class="btn btn-primary btn-sm btn-rounded btn-outline"><i class="fa fa-file"></i> {{$file->filename}} ({{number_format($file->size / 1024, 2)}} KB)</a>
                </div>
                @empty
                <div class="col-xs-12 m-t-10">
                    <span id="totalUploadedFiles">No files uploaded!</span>
                </div>
                @endforelse


                <div class="col-xs-12 m-t-15">
                    <h5>@lang('modules.tasks.comment')</h5>
                </div>

                <div class="col-xs-12" id="comment-container">
                    <div id="comment-list">
                        <div class="row b-b m-b-5 font-12">
                            <div class="col-xs-12">
                                <h5> <span class="text-muted font-12">Name</span></h5>
                            </div>
                            <div class="col-xs-10">
                                Comment Here
                            </div>
                            <div class="col-xs-2 text-right">
                                <a href="javascript:;" data-comment-id="" class="text-danger" onclick="deleteComment('');return false;">@lang('app.delete')</a>
                            </div>
                        </div>
                        
                        <div class="col-xs-12">
                            @lang('messages.noRecordFound')
                        </div>
                    </div>
                </div>

                <div class="form-group" id="comment-box">
                    {!! Form::open(['id'=>'Comment','method'=>'POST', 'files' => true]) !!}
                    <div class="col-xs-12">
                        <textarea name="comment" id="task-comment" class="summernote" placeholder="@lang('modules.tasks.comment')"></textarea>
                    </div>
                    <div class="col-xs-12">
                        <a href="javascript:;" id="submit-comment" class="btn btn-info btn-sm"><i class="fa fa-send"></i> @lang('app.submit')</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-xs-12" id="task-history-section">
        </div>

    </div>

</div>
<script src="{{ asset('plugins/bower_components/summernote/dist/summernote.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="{{ asset('plugins/bower_components/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/peity/jquery.peity.init.js') }}"></script>

<script>
    function downloadFile(id) {
        var url = '{{route('member.article.downloadFile',':id')}}';
        var url = url.replace(':id', id);
        window.location.href = url;
    }

    function viewTask(id) {
        $(".right-sidebar").slideDown(50).addClass("shw-rside");
        var url = "{{ route('member.all-tasks.show',':id') }}";
        url = url.replace(':id', id);
        $.easyAjax({
            type: 'GET',
            url: url,
            success: function (response) {
                if (response.status == "success") {
                    console.log(response.article);
                    $('#right-sidebar-content').html(response.view);
                }
            }
        });
    }

    function markComplete(status) {
        var id = '{{$article->id}}';
        if (status ==='complete') {var status = 1;} else {var status = 0;}
        var url = "{{ route('member.article.updateStatus',['id' => ':id', 'status' => ':status']) }}?_token={{csrf_token()}}";
        var url = url.replace(':id', id).replace(':status', status);
        $.easyAjax({
            type: 'POST',
            url: url,
            success: function (response) {
                var id = '{{$article->id}}';
                var url = "{{ route('member.article.show',':id') }}";
                url = url.replace(':id', id);
                $.easyAjax({
                    type: 'GET',
                    url: url,
                    success: function (response) {
                        if (response.status == "success") {
                            $('#right-sidebar-content').html(response.view);
                        }
                    }
                });
                // if (response.status == "success") {
                //     $('#right-sidebar-content').html(response.view);
                // }
            }
        });
    }
</script>
<script type="text/javascript">
        $('.summernote').summernote({
        height: 100,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: false,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
            ]
        })
</script>
