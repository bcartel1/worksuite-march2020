@extends('layouts.app')

@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-8 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }} </h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-4 col-sm-8 col-md-8 col-xs-12 text-right">
        <a href="{{ route('admin.employees.create') }}" class="btn btn-outline btn-success btn-sm pull-right">@lang('article::app.addwriters') <i class="fa fa-plus" aria-hidden="true"></i></a>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li class="active">{{ $pageTitle }}</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection

@push('head-script')
<link href="{{url('/css/user-card.css')}}" rel="stylesheet">
<style type="text/css">
    .filter-section>h5{
        display:none;
    }
</style>
@endpush

@section('content')

<div class="row dashboard-stats" style="margin-left: 10px;">
    <div class="col-md-12 m-b-30">
        <div class="white-box">

            @section('filter-section')
            <div class="row">
                <h1>{{$writer->name}}</h1>
            </div>
            @endsection

            <div class="card-columns widget-app-columns">
                <div class="card">
                        <img class="card-img img-responsive" src="../assets/images/big/img1.jpg" alt="Card image">
                        <div class="card-img-overlay card-inverse social-profile-first bg-over">
                            <img src="../assets/images/users/1.jpg" class="img-circle" width="100">
                            <h4 class="card-title">John doe</h4>
                        </div>
                        <div class="card-body text-center">
                            <div class="row">
                                <div class="col">
                                    <h3 class="m-b-0">12K</h3>
                                    <h5 class="font-light">Followers</h5>
                                </div>
                                <div class="col">
                                    <h3 class="m-b-0">420</h3>
                                    <h5 class="font-light">Following</h5>
                                </div>
                                <div class="col">
                                    <h3 class="m-b-0">128</h3>
                                    <h5 class="font-light">Tweets</h5>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
    </div>
</div>
</div>
    @endsection

    @push('footer-script')

    @endpush
