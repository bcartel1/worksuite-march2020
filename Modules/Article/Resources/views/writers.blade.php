@extends('layouts.app')

@section('page-title')
<div class="row bg-title">
    <!-- .page title -->
    <div class="col-lg-8 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title"><i class="{{ $pageIcon }}"></i> {{ $pageTitle }} </h4>
    </div>
    <!-- /.page title -->
    <!-- .breadcrumb -->
    <div class="col-lg-4 col-sm-8 col-md-8 col-xs-12 text-right">
        <a href="{{ route('admin.employees.create') }}" class="btn btn-outline btn-success btn-sm pull-right">@lang('article::app.addwriters') <i class="fa fa-plus" aria-hidden="true"></i></a>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin.dashboard') }}">@lang('app.menu.home')</a></li>
            <li class="active">{{ $pageTitle }}</li>
        </ol>
    </div>
    <!-- /.breadcrumb -->
</div>
@endsection

@push('head-script')
<link href="{{url('/css/user-card.css')}}" rel="stylesheet">
<style type="text/css">
    .filter-section>h5{
        display:none;
    }
</style>
@endpush

@section('content')

<div class="row dashboard-stats" style="margin-left: 10px;">
    <div class="col-md-12 m-b-30">
        <div class="white-box">

            @section('filter-section')
            <div class="row">
                <h1>@lang('article::app.writers')</h1>
            </div>
            @endsection

            <div class="col-sm-6">
                <h4>
                    <span class="text-info">{{ $totalWriters ?? '0' }}</span>
                    <span class="font-12 text-muted m-l-5"> @lang('article::app.totalwriters')</span></h4>
            </div>
        </div>
    </div>


    <div class="row el-element-overlay">
        @foreach ($writers as $writer)
        <div class="col-lg-2 col-md-4">
            <div class="card" style="border: 1px solid rgba(0,0,0,0.2); border-radius: 10px;">
                <div class="el-card-item">
                    <div class="el-card-avatar el-overlay-1">
                        <img src="@if(is_null($writer->image)){{ asset('img/default-profile-3.png') }} @else {{ asset_url('avatar/'.$writer->image) }} @endif" alt="user" style="border-radius: 10px 10px 0px 0px; height: 150px; width: cover;">
                        <div class="el-overlay scrl-up" style="border-radius: 10px 10px 0px 0px;">
                            <ul class="el-info" style="border-radius: 10px 10px 0px 0px;">
                                <li>
                                    <a class="btn default btn-outline" href="{{route('admin.article.writers')}}/{{$writer->id}}">
                                        <i class="icon-magnifier"></i>
                                    </a>
                                </li>
                                <li>
                                    <a class="btn default btn-outline" href="./{{$writer->id}}/edit">
                                        <i class="icon-pencil"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="el-card-content">
                        <h4 class="box-title">{{$writer->name}}</h4>
                        <i class="fa fa-star" style="color: yellow;"></i>
                        <i class="fa fa-star" style="color: yellow;"></i>
                        <i class="fa fa-star" style="color: yellow;"></i>
                        <i class="fa fa-star" style="color: yellow;"></i>
                        <i class="fa fa-star"></i>
                        <br> </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
    @endsection

    @push('footer-script')

    @endpush
