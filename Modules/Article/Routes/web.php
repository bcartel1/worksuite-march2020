<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admin Route
Route::prefix('member')->group(function() {
	
	//Articles
	Route::get('article-management', 'ArticleController@index')->name('member.article.index');
	Route::get('article-management/create', 'ArticleController@create')->name('member.article.create');
	Route::post('article-management/create', 'ArticleController@store')->name('member.article.store');
	Route::get('article-management/article/{id}', 'ArticleController@show')->name('member.article.show');
	Route::get('article-management/article/{id}/edit', 'ArticleController@edit')->name('member.article.edit');
	Route::post('article-management/article/{id}/edit', 'ArticleController@update')->name('member.article.update');
	Route::post('article-management/article/{id}/update/{status}', 'ArticleController@updateStatus')->name('member.article.updateStatus');
		Route::delete('article-management/article/{id}/delete', 'ArticleController@destroy')->name('member.article.delete');

	//Article Files
	Route::post('article-management/store-file', 'ArticleFileController@store')->name('member.article.storeFiles');
	Route::get('article-management/article-file/{id}', 'ArticleFileController@download')->name('member.article.downloadFile');
	Route::post('article-management/article-file/removearticle/{id}', 'ArticleFileController@removeArticle')->name('member.article.removeArticle');
	Route::post('article-management/article-file/{id}/delete', 'ArticleFileController@destroy')->name('member.article.deleteFile');

    //Type
    Route::get('article-management/type', 'AdminArticleController@typeView')->name('member.article.typeView');
	Route::post('article-management/type', 'AdminArticleController@createType')->name('member.article.createType');

    //Settings
	Route::get('article-management/settings', 'AdminArticleController@settings')->name('member.article.settings');
	Route::post('article-management/settings', 'AdminArticleController@storeSettings');
	Route::get('article-management/writers', 'AdminArticleController@writers')->name('member.article.writers');
	Route::get('article-management/writers/{id}', 'AdminArticleController@writerView');
});

