<?php

return [
    'name' => 'Asset',
    'verification_required' => true,
    'envato_item_id' => '25798074',
    'script_name' => 'worksuite-asset',
    'setting' => \Modules\Asset\Entities\AssetSetting::class,
];
