<?php

namespace App\Listeners;

use App\Events\TicketReplyEvent;
use App\Notifications\NewTicketReply;
use Illuminate\Support\Facades\Notification;

class TicketReplyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TicketReplyEvent $event
     * @return void
     */
    public function handle(TicketReplyEvent $event)
    {
        Notification::send($event->notifyUser, new NewTicketReply($event->ticketReply));
    }
}
