<?php

namespace App\Observers;

use App\Events\NewInvoiceEvent;
use App\Invoice;
use App\UniversalSearch;
use App\User;

class InvoiceObserver
{

    public function created(Invoice $invoice)
    {
        if (!isRunningInConsoleOrSeeding() ) {
            if (($invoice->project && $invoice->project->client_id != null) || $invoice->client_id != null) {
                $clientId = ($invoice->project && $invoice->project->client_id != null) ? $invoice->project->client_id : $invoice->client_id;
                // Notify client
                $notifyUser = User::withoutGlobalScope('active')->findOrFail($clientId);
                event(new NewInvoiceEvent($invoice, $notifyUser));
            }
        }
    }

    public function deleting(Invoice $invoice){
        $universalSearches = UniversalSearch::where('searchable_id', $invoice->id)->where('module_type', 'invoice')->get();
        if ($universalSearches){
            foreach ($universalSearches as $universalSearch){
                UniversalSearch::destroy($universalSearch->id);
            }
        }
    }

}
