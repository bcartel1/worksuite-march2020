<?php

namespace App\Observers;

use App\Events\NewProjectMemberEvent;
use App\ProjectMember;

class ProjectMemberObserver
{
    public function created(ProjectMember $member)
    {
        if (!isRunningInConsoleOrSeeding() ) {
            event(new NewProjectMemberEvent($member));
        }
    }
}
