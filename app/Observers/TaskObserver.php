<?php

namespace App\Observers;

use App\Events\TaskEvent;
use App\Events\TaskUpdated as EventsTaskUpdated;
use App\Http\Controllers\Admin\AdminBaseController;
use App\Notifications\NewTask;
use App\Task;
use App\TaskboardColumn;
use App\Traits\ProjectProgress;
use App\UniversalSearch;
use App\User;
use Illuminate\Support\Facades\Notification;

class TaskObserver
{

    use ProjectProgress;
    public function saving(Task $task)
    {
        // $user = auth()->user();
        // $pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), array('cluster' => 'ap2', 'useTLS' => true));
        // $pusher->trigger('task-updated-channel', 'task-updated', $user);
    }

    public function creating(Task $task)
    {
        if (!isRunningInConsoleOrSeeding()) {
            $user = auth()->user();
            //         Cannot put in creating, because saving is fired before creating. And we need company id for check bellow
            if ($user) {
                $task->created_by = $user->id;
            }
        }
    }

    public function created(Task $task)
    {
        if (!isRunningInConsoleOrSeeding()) {
            if (request()->has('project_id') && request()->project_id != "all" && request()->project_id != '') {
                if ($task->project->client_id != null && $task->project->allow_client_notification == 'enable' && $task->project->client->status != 'deactive') {
                    event(new TaskEvent($task, $task->project->client, 'NewClientTask'));
                }
            }
            $log = new AdminBaseController();
            if (\user()) {
                $log->logTaskActivity($task->id, user()->id, "createActivity", $task->board_column_id);
            }

            if ($task->project_id) {
                //calculate project progress if enabled
                $log->logProjectActivity($task->project_id, __('messages.newTaskAddedToTheProject'));
                $this->calculateProjectProgress($task->project_id);
            }

            //log search
            $log->logSearchEntry($task->id, 'Task: ' . $task->heading, 'admin.all-tasks.edit', 'task');


            //Send notification to user
            event(new TaskEvent($task, $task->users, 'NewTask'));
        }
    }

    public function updated(Task $task)
    {
        if (!isRunningInConsoleOrSeeding()) {

            if ($task->isDirty('board_column_id')) {

                $taskBoardColumn = TaskboardColumn::findOrFail($task->board_column_id);

                if ($taskBoardColumn->slug == 'completed') {
                    // send task complete notification
                    event(new TaskEvent($task, $task->users, 'TaskUpdated'));

                    $admins = User::allAdmins();
                    event(new TaskEvent($task, $admins, 'TaskCompleted'));

                    if ((request()->project_id && request()->project_id != "all") || (!is_null($task->project))) {
                        if ($task->project->client_id != null && $task->project->allow_client_notification == 'enable' && $task->project->client->status != 'deactive') {
                            event(new TaskEvent($task, $task->project->client, 'TaskCompleted'));
                        }
                    }
                }
            }

            if (request('user_id')) {
                //Send notification to user
                event(new TaskEvent($task, $task->users, 'TaskUpdated'));

                if ((request()->project_id != "all") && !is_null($task->project)) {
                    if ($task->project->client_id != null && $task->project->allow_client_notification == 'enable' && $task->project->client->status != 'deactive') {
                        event(new TaskEvent($task, $task->project->client, 'TaskUpdatedClient'));
                    }
                }
            }
        }

        if (!request()->has('draggingTaskId') && !request()->has('draggedTaskId')) {
            event(new EventsTaskUpdated($task));
        }

        if (\user()) {
            $log = new AdminBaseController();
            $log->logTaskActivity($task->id, user()->id, "updateActivity", $task->board_column_id);
        }

        if ($task->project_id) {
            //calculate project progress if enabled
            $this->calculateProjectProgress($task->project_id);
        }
    }

    public function deleting(Task $task)
    {
        $universalSearches = UniversalSearch::where('searchable_id', $task->id)->where('module_type', 'task')->get();
        if ($universalSearches) {
            foreach ($universalSearches as $universalSearch) {
                UniversalSearch::destroy($universalSearch->id);
            }
        }
    }
}
