<?php

namespace App\Observers;

use App\Events\TicketReplyEvent;
use App\TicketReply;
use Illuminate\Support\Facades\Auth;

class TicketReplyObserver
{
    public function created(TicketReply $ticketReply)
    {
        if (!isRunningInConsoleOrSeeding()) {
            if(Auth::user()->hasRole('client') && !is_null($ticketReply->ticket->agent)){
                event(new TicketReplyEvent($ticketReply, $ticketReply->ticket->agent));
            }
            else{
                event(new TicketReplyEvent($ticketReply, $ticketReply->ticket->client));
            }
        }
    }
}
